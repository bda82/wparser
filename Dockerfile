FROM ruby:2.5

ENV RAILS_ENV='development'
ENV RACK_ENV='development' 

WORKDIR /var/www/wparser

RUN apt-get update -qq && apt-get install -y nodejs postgresql-client

COPY src/Gemfile /var/www/wparser/Gemfile
COPY src/Gemfile.lock /var/www/wparser/Gemfile.lock
COPY src /var/www/wparser

RUN bundle install

COPY docker-entrypoint.sh /var/www/wparser/docker-entrypoint.sh

RUN chmod +x /var/www/wparser/docker-entrypoint.sh

ENTRYPOINT ["/var/www/wparser/docker-entrypoint.sh"]

EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]