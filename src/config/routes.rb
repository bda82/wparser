Rails.application.routes.draw do
  get 'parser/index' => 'parser#index'
  get 'parser/get' => 'parser#get'
  get 'welcome/index'

  root 'welcome#index'
end
