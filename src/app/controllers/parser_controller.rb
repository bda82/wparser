require 'open-uri'
require 'nokogiri'

class ParserController < ApplicationController
  def initialize
    @url = "https://en.wikipedia.org/wiki/Medical_procedure#List_of_medical_procedures"
    # Добавил это сюда, так как в ином случае нужно делать cron job для 
    # обновления данных страницы на предмет появления новых ссылок
    # и добавлять два контейнера celery/celery-beat,
    # ну или использовать гем whenever
    html = open(@url)
    doc = Nokogiri::HTML(html)
    # Знаю, что нужно regexp, но...
    # Также проскакивают служебные ссылки типа "Learn about Wikipedia and how it works"
    @allLinks = doc.css('a').map { |link| { 'title' => link['title'], 'href' => link['href'] } if !link['title'].nil? and !link['href'].nil? and !link['title'].empty? and !link['href'].to_s.strip.empty? and link['href'].include? '/wiki/' and !link['href'].include? 'https://' and !link['href'].include? '//' and !link['title'].include? 'ISBN' and !link['title'].include? ':' and !link['title'].include? '[' }.compact
    updateDatabase
  end

  # GET /parser/index
  def index
    render json: @allLinks.to_json
  end

  # GET /parser/get
  def get
    requestedLinks = createLinksToOutput params[:title]
    render json: { "allLinksCount" => Link.count, "requestedLinks" => requestedLinks }.to_json
  end

  private
    def updateDatabase
      @allLinks.each do |link|
        createLinkIfNotExistInDatabase link
      end
    end

  private
    def createLinkIfNotExistInDatabase(link)
      if !Link.exists?(title: link['title'])
        newLink = Link.new
        newLink.title = link['title']
        newLink.href = link['href']
        newLink.save
      end
    end

  private
    def createLinksToOutput(searchParams)
      result = []
      
      recordsStartWith = Link.where("lower(title) like ?", "#{searchParams.downcase}%")

      recordsContains = Link.where("lower(title) like ?", "%#{searchParams.downcase}%")

      result = recordsStartWith + recordsContains

      return result
    end
end
