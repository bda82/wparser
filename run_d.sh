#!/bin/bash

set -e

echo "Start Test server..."

docker-compose -f docker-compose.yaml up --build -d